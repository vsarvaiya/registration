const bcrypt = require("bcrypt");
const pool = require("../db");
const GenerateJwt = require("../util/GenerateJwt");

//FORGOT PASSWORD CONTROLLER
module.exports.forgot_password = async (req, res) => {
  const { email } = req.body;
  const user = await pool.query("SELECT * FROM demousers WHERE email=$1", [email]);

  //check email exists or not
  if (user.rows.length === 0) {
    return res.status(404).json({
      error: "Sorry! An account with that email address doesn't exist",
    });
  } else {
    return res.status(200).json({
      email: user.rows[0].email,
    });
  }
};

//RESET PASSWORD CONTROLLER
module.exports.reset_password = async (req, res) => {
  try {
    const { email, password} = req.body;
    const user = await pool.query("SELECT * FROM demousers WHERE email=$1", [
      email,
    ]);

    if (user.rows.length === 0) {
      return res.status(404).json({
        error: "Sorry! An account with that email address doesn't exist",
      });
    } else {
      //password encryption
      const newPassword = req.body;
      const hashPassword = bcrypt.hashSync(newPassword.password, 8);
      newPassword.password = hashPassword;
      console.log(hashPassword);
      bcrypt.hash(password, 10, async (err, hashPassword) => {
        //error occurs during encryption
        if (err) {
          return res.status(400).json({
            error:
              "An error occured while reseting your password. Please try again!",
          });
        } else {
            // await pool.query("UPDATE demousers SET password= " +hashPassword+ " WHERE email=$1", [
            await pool.query("UPDATE demousers SET password=$2 WHERE email=$1", [
            email,
            hashPassword,
          ]);
          //generate token
          const token = GenerateJwt({
            user_id:user.rows[0].user_id,
            email:user.rows[0].email,
        });

          res.status(200).json({
            password: "Password updated successfully!",
            token,
          });
        }
      });
    }
  } catch (err) {
    return res.status(400).json({
      error: err.message,
    });
  }
};
