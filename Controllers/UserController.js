const bcrypt = require("bcrypt");
const pool = require("../db");
const GenerateJwt = require("../util/GenerateJwt");
const { config } = require("../config");

//USER REGISTRATION CONTROLLER
module.exports.userRegister = async (req, res) => {
  try {
    let { firstname, lastname, email, phone, password } = req.body;
    const user = await pool.query("SELECT * FROM demousers WHERE email=$1", [
      email,
    ]);
      //encrypt the password
      bcrypt.hash(password, 10, async (err, hashedPassword) => {

       
          //convert the firstname and lastname to be capitalized i.e  (first letter capital and the rest small letters)
          firstname = firstname[0].toUpperCase() + firstname.slice(1);
          lastname = lastname[0].toUpperCase() + lastname.slice(1);

          const newUser = await pool.query(
            "INSERT INTO demousers (firstname, lastname, email, phone, password) VALUES($1, $2, $3, $4, $5) RETURNING *",
            [firstname, lastname, email, phone, hashedPassword]
          );
          //generate token
           const token = GenerateJwt(
            {
                user_id:newUser.rows[0].user_id,
                firstname:newUser.rows[0].firstname,
                lastname:newUser.rows[0].lastname,
                 email:newUser.rows[0].email, 
                 phone:newUser.rows[0].phone,
          }, config.secretKey
        ); 

          res.status(400).json({
            message: `Account created successfully!`,
            token,
          });
      });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({
      error: err.message,
    });
  }
};



//USER LOGIN CONTROLLER

module.exports.userLogin = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await pool.query("SELECT * FROM demousers WHERE email=$1", [
      email,
    ]);
    if (user.rows.length === 0) {
      res.status(404).json({
        error: "Sorry! An account with that email doesn't exist!",
      });
    } else {
      bcrypt.compare(password, user.rows[0].password, (err, validPassword) => {
        if (err) {
          res.status(401).json({
            error: "Sorry! Email or password is incorrect",
          });
        } else if (validPassword) {
          //generate a token
          const token = GenerateJwt({
            user_id:user.rows[0].user_id,
            email:user.rows[0].email,
        });

          res.json({
            message: "Login successfully!",
            token,
          });
        } else {
          res.status(401).json({
            error: "Sorry! Email or password is incorrect",
          });
        }
      });
    }
  } catch (err) {
    console.log(err.message);
    res.status(500).json({
      error: err.message,
    });
  }
};

//USER TOKEN VERIFY

module.exports.user_token_verify = async (req, res) => {
  try {
    res.status(200).json({ authorized: true });
  } catch (err) {
    res.status(500).json({
      error: err.message,
    });
  }
};
