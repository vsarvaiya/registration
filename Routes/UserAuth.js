const router = require("express").Router();
const validateInfo = require("../Middleware/ValidateInfo");
const authorization = require("../Middleware/Authorization");
const controller = require("../Controllers/UserController");
const passwordController = require("../Controllers/PasswordController");

//REGISTER
router.post("/register", validateInfo, controller.userRegister);

//LOGIN
router.post("/login", validateInfo, controller.userLogin);

//USER TOKEN
router.get("/verify-token", authorization, controller.user_token_verify);

//PASSWORD
router.post("/forgot-password", passwordController.forgot_password);
router.patch("/reset-password", passwordController.reset_password);

module.exports = router;