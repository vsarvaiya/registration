const express = require("express");
const app = express();
const cors = require("cors");

const PORT = 3000;

app.use(express.json()); 
app.use(cors()); 

// app.use("/auth", require("./Routes/UserAuth"));
app.use(require("./Routes/UserAuth"));

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});