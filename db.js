const { Pool } = require("pg");
const AWS = require("aws-sdk");
const { config } = require("./config");

AWS.config.update({
  region: config.region,
  accessKeyId: config.accessKeyId,
  secretAccessKey: config.secretAccessKey,
});

const pgPool = new Pool({
  host: config.host,
  user: config.username,
  database: config.database,
  password: config.password,
  port: config.port,
});

module.exports = {
  query: async (text, params, callback) => {
    try {
      const data = await pgPool.query(text, params, callback);
      return data;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log("Query Error====", error.message);
    }
  },
};